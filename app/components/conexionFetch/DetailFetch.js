import React, { Component } from 'react';
import {View,StyleSheet,Text,Image} from 'react-native';

export default class ConexionFetch extends Component{
    constructor(props){
        super(props);

        this.state = {
            textValue: 0,
            count: 0,
            items: [],
            error: null,
            id: this.props.route.params.itemId,
        };
    }
    async componentDidMount() {
        await fetch(`https://yts.mx/api/v2/movie_details.json?movie_id=${this.state.id}`)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        items: result.data.movie,
                    });
                },
                error => {
                    this.setState({
                        error: error,
                    });
                },
            );
    }

    render(){
        return(
            <View style={styles.container}>
                <View>
                    <Text></Text>
                    <Image source={{uri: this.state.items.medium_cover_image}} style={styles.itemImage} resizeMode="cover"/>
                </View>

                <View style={styles.text}>
                    <View>
                        <View>
                            <View>
                                <Text style={styles.title}>{this.state.items.title}
                                <Text style={styles.year}> ({this.state.items.year})</Text></Text>
                                
                            </View>

                        </View>
                        <Text style={styles.txt1}>Descripción:</Text>
                        <Text style={styles.description}>{this.state.items.description_intro}</Text>
                        <Text style={styles.txt1}>Géneros:</Text>
                        <Text style={styles.genre}>{this.state.items.genres+','}</Text>
                       
                    </View>
                </View>
                
                
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#BBDEFB',
    },
    itemContainer:{
        backgroundColor:'#BBDEFB',
        padding:20,
        marginVertical:8,
        marginHorizontal:16,
    },
    title:{
        fontSize:45,
        paddingLeft:10,
    },

    description:{
        fontSize:17,
        paddingLeft:20,
        textAlign:'justify',
        paddingRight:20,
    },
    itemImage:{
        height:400,
        width:250,
        alignSelf:'center',
    },
    year:{
        fontSize:25,
    },
    txt1:{
        fontSize:35,
        paddingLeft:10,
        fontFamily:'cursive',

    },
    genre:{
        paddingLeft:20,
        textAlign:'justify',
        fontSize:17,
    },

});