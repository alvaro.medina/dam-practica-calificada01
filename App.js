import React, {Component} from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import ConexionFetch from './app/components/conexionFetch/ConexionFetch';
import DetailFetch from './app/components/conexionFetch/DetailFetch';


const Stack = createStackNavigator();

export default class App extends Component{
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen 
            name="Home" 
            component={ConexionFetch} 
            options={{
              title: 'Lista de Peliculas',
              headerStyle: {
                backgroundColor: '#7E57C2',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontFamily: 'cursive',
                color: '#fff',
                fontSize: 45,
              },
            }}
          />
          <Stack.Screen 
            name="Details" 
            component={DetailFetch}
            options={{
              title: 'Detalles de la Pelicula',
              headerStyle: {
                backgroundColor: '#7E57C2',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontFamily: 'cursive',
                color: '#fff',
                fontSize: 45,
              },
            }} 
          />

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
